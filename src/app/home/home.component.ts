import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public holidays: Holiday[] = [];

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.httpClient.get<Holiday[]>('https://date.nager.at/api/v2/publicholidays/2022/FR').subscribe(
      (data: Holiday[]) => {
        this.holidays = data;
        console.log(this.holidays);
      }
    )
  }

}

interface Holiday {
  counties: string;
  countryCode: string;
  date: string;
  fixed: boolean;
  global: boolean;
  launchYear: number;
  localName: string;
  name: string;
  type: string;
}
